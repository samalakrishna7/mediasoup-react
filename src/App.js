import React, { useState, useRef } from 'react';
import './App.css';
import * as mediasoupClient from 'mediasoup-client';
import io from 'socket.io-client';
import { socketPromise } from './services/socketRequest';
let socket;
let device;
let RouterRtpCapabilities;
let producer;
const App = () => {
  const [connectUser, setConnectUser] = useState(false);
  // const [webcam, setWebCam] = useState(false);

  //videos
  const [userVideo, setUserVideo] = useState();
  const [subVideo, setSubVideo] = useState();
  //
  console.log(userVideo, subVideo, RouterRtpCapabilities, producer);
  const videoRef1 = useRef(null);
  const videoRef2 = useRef(null);

  const opts = {
    path: '/server',
    transports: ['websocket'],
  };
  const serverUrl = `https://plsms.pulse.stream/`;

  const connectSocket = async () => {
    console.log('in connect');
    socket = io(serverUrl, opts);
    socket.request = socketPromise(socket);
    console.log('Request accepted');
    console.log(socket.request);
    socket.on('connect', async () => {
      console.log('Socket In Connect .. connected!');
      const data = await socket.request('getRouterRtpCapabilities');
      console.log('Got Params...');
      try {
        device = new mediasoupClient.Device();
      } catch (error) {
        if (error.name === 'UnsupportedError') {
          console.error('browser not supported');
        }
      }
      await device.load({ routerRtpCapabilities: data });
      setConnectUser(true);
    });
  };

  const publish = async (e) => {
    console.log('in publish');
    console.log(e.target.id);
    let webcam;
    if (e.target.id === 'webcam') {
      webcam = true;
      console.log('true');
    }
    const data = await socket.request('createProducerTransport', {
      forceTcp: false,
      rtpCapabilities: device.rtpCapabilities,
    });
    if (data.error) {
      console.error(data.error);
      return;
    }
    console.log('created producer transport');
    const transport = device.createSendTransport(data);
    transport.on('connect', async ({ dtlsParameters }, callback, errback) => {
      console.log('send transport created');

      socket
        .request('connectProducerTransport', { dtlsParameters })
        .then(callback)
        .catch(errback);
    });

    transport.on(
      'produce',
      async ({ kind, rtpParameters }, callback, errback) => {
        try {
          console.log('producer created');

          const { id } = await socket.request('produce', {
            transportId: transport.id,
            kind,
            rtpParameters,
          });
          callback({ id });
        } catch (err) {
          errback(err);
        }
      }
    );

    transport.on('connectionstatechange', async (state) => {
      console.log('in connection change');
      switch (state) {
        case 'connecting':
          console.log('connecting');
          break;
        case 'connected':
          console.log('connecting stream');
          let streams = await stream;
          videoRef1.current.srcObject = streams;
          setUserVideo(streams);
          break;
        case 'failed':
          transport.close();
          break;
        default:
          break;
      }
    });
    let stream;
    try {
      stream = await getUserMedia(transport, webcam);
      const track = stream.getVideoTracks()[0];
      const params = { track };
      console.log(params);
      console.log('in media');
      producer = await transport.produce(params);
      console.log(' transport produced..final');
    } catch (err) {
      console.log('failed to connect');
    }
  };

  async function getUserMedia(transport, isWebcam) {
    console.log('in user media');
    if (!device.canProduce('video')) {
      console.error('cannot produce video');
      return;
    }
    let stream;
    try {
      stream = isWebcam
        ? await navigator.mediaDevices.getUserMedia({ video: true })
        : await navigator.mediaDevices.getDisplayMedia({ video: true });
    } catch (err) {
      console.error('getUserMedia() failed:', err.message);
      throw err;
    }
    console.log('returning user media');
    return stream;
  }

  const subscribe = async () => {
    const data = await socket.request(
      'createConsumerTransport',
      {
        forceTcp: false,
      },
      async (data) => {
        console.log(data);
      }
    );
    const transport = device.createRecvTransport(data);
    transport.on('connect', ({ dtlsParameters }, callback, errback) => {
      socket
        .request('connectConsumerTransport', {
          transportId: transport.id,
          dtlsParameters,
        })
        .then(callback)
        .catch(errback);
    });

    transport.on('connectionstatechange', async (state) => {
      switch (state) {
        case 'connecting':
          break;

        case 'connected':
          let streams = await stream;
          videoRef2.current.srcObject = streams;

          setSubVideo(streams);
          console.log(streams);
          await socket.request('resume');
          break;

        case 'failed':
          transport.close();

          break;

        default:
          break;
      }
    });

    const stream = consume(transport);
    if (data.error) {
      console.error(data.error);
      return;
    }
  };

  async function consume(transport) {
    const { rtpCapabilities } = device;
    console.log('in consume');
    const data = await socket.request('consume', { rtpCapabilities });
    console.log(data);
    const { producerId, id, kind, rtpParameters } = data;

    let codecOptions = {};
    const consumer = await transport.consume({
      id,
      producerId,
      kind,
      rtpParameters,
      codecOptions,
    });
    const stream = new MediaStream();
    stream.addTrack(consumer.track);
    return stream;
  }
  React.useEffect(() => {
    return () => {
      socket.emit('connection');
      socket.off();
    };
  }, []);
  return (
    <div className='App'>
      <div className='appbox'>
        <p className='p'>User 1</p>
        <video autoPlay controls className='video-div' ref={videoRef1}></video>
        <button
          className='yellow-button'
          onClick={(e) => {
            connectSocket();
          }}
        >
          {connectUser ? 'Connected' : 'Connect'}
        </button>
      </div>
      <div className='appbox'>
        <button
          className='yellow-button'
          onClick={(e) => {
            publish(e);
          }}
        >
          Share Screen{' '}
        </button>
        <button
          id='webcam'
          className='yellow-button'
          onClick={(e) => {
            publish(e);
          }}
        >
          Start webcam{' '}
        </button>
      </div>
      <div className='appbox'>
        <p className='p'>User 2</p>
        <video autoPlay controls className='video-div' ref={videoRef2}></video>
        <button
          className='yellow-button'
          onClick={(e) => {
            subscribe(e);
          }}
        >
          Subscribe{' '}
        </button>
      </div>
    </div>
  );
};

export default App;
